from django.shortcuts import redirect


class ModeratorOnlyMixin:
    def dispatch(self, request, *args, **kwargs):
        if request.user.profile is not None and request.user.profile.is_moderator:
            return super().dispatch(request, *args, **kwargs)
        else:
            return redirect('quiz:index')

from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import View, DetailView
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.mixins import LoginRequiredMixin
from users.forms import UserSignUpForm
from django.contrib.auth.models import User 
from users.models import Profile

# Create your views here.
class UserSignUpView(View):

    def get(self, request):
        form = UserSignUpForm()
        return render(request, 'registration/signup.html', {"form": form})

    def post(self, request):
        form = UserSignUpForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("/")


class UserLoginView(LoginView):
    template_name = 'login.html'
    redirect_field_name = 'next'

    def get_success_url(self):
        return reverse_lazy('quiz:index')


class UserLogoutView(LogoutView):
    next_page = reverse_lazy('quiz:index')


class UserProfileView(LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('users:login')

    model = Profile
    context_object_name = 'profile'

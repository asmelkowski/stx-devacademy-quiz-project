from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from quiz.models import Quiz, QuizScore, ViolationNotification


# Create your models here.
# TODO
# Add some fields like: total score, list of quizes, ...
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")
    is_moderator = models.BooleanField(default=False)
    total_score = models.FloatField(default=0.0)

    def quizes_taken(self):
        return QuizScore.objects.filter(user=self.user).all()

    def quizes_created(self):
        return Quiz.objects.filter(author=self.user).all()

    def calculate_score(self):
        user_scores = QuizScore.objects.filter(user=self.user).all()
        user_total = 0.0
        for score in user_scores:
            user_total += score.score_value
        self.total_score = user_total
        self.save()

    def assigned_notifications(self, active=True):
        if not self.is_moderator:
            return None
        closed = not active
        return ViolationNotification.objects.filter(assigned_to=self.user).filter(is_closed=closed).all()


@receiver(post_save, sender=User)
def create_profile_for_new_user(sender, created, instance, **kwargs):
    if created:
        profile = Profile(user=instance)
        profile.save()

from django.urls import path, include
from users.views import UserSignUpView, UserLoginView, UserLogoutView, UserProfileView

urlpatterns = [
    path("", include("django.contrib.auth.urls")),
    path("signup", UserSignUpView.as_view(), name="user_signup"),
    path("login", UserLoginView.as_view(), name="login"),
    path("logout", UserLogoutView.as_view(), name="logout"),
    path("profile/<int:pk>", UserProfileView.as_view(), name="user-profile"),
]
window.onload = function() {
  var body_element = document.querySelector("body")
  var dark_switch = document.querySelector("button#dark-switch")
  var dropdown_icon = document.querySelector("svg#dropdown-icon")
  var dropdown_menu = document.querySelector("div.dropdown-menu")
  dark_switch.addEventListener("click", () => {
      body_element.classList.toggle("dark");
  })
  dropdown_icon.addEventListener("click", () => {
      dropdown_menu.classList.toggle("menu__active");
  })
}
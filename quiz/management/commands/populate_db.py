import requests
from urllib import parse
from django.core.management.base import BaseCommand
from quiz.models import Category, Quiz, Question, Option
from django.contrib.auth.models import User


class Command(BaseCommand):
    difficulty_levels = {
        'easy': 0,
        'medium': 1,
        'hard': 2,
    }

    cateogries_range = (9, 32)
    base_url = 'https://opentdb.com/api.php?'

    def create_admin(self):
        admin = User.objects.filter(
            username='admin').first()
        if not admin:
            admin = User.objects.create_superuser(
                username='admin', password='e7ELMMDrBPYRDSzrWXnV')
        return admin

    def populate_quizes(self, admin, question_qty, category):
        flag = True
        params = {
            'amount': question_qty,
            'category': category
        }
        quiz_json = requests.get(
            self.base_url+parse.urlencode(params)).json()['results']
        try:
            quiz_category, quiz_title = quiz_json[0]['category'].split(":")
            diff_lvl = quiz_json[0]['difficulty']
        except ValueError:
            flag = False
        if flag:
            new_category, _ = Category.objects.get_or_create(
                name=quiz_category)
            new_quiz = Quiz(
                title=quiz_title,
                author=admin,
                difficulty=self.difficulty_levels[diff_lvl]
            )
            new_quiz.save()
            new_quiz.categories.add(new_category)
            for question in quiz_json:
                new_question = Question(
                    question_text=question['question'],
                    quiz=new_quiz
                )
                new_question.save()
                to_add_options = [Option(
                    answer_text=opt,
                    is_correct=False,
                    question=new_question
                )
                for opt in question['incorrect_answers']]
                to_add_options.append(Option(
                    answer_text=question['correct_answer'],
                    is_correct=True,
                    question=new_question
                ))
                Option.objects.bulk_create(to_add_options)

    def handle(self, *args, **options):
        admin = self.create_admin()
        for i in range(*self.cateogries_range):
            self.populate_quizes(admin, 10, i)

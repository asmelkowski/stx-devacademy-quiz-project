from django.db import models
from django.db.models import F
from django.contrib.auth.models import User


# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return f"Category {self.name}"


class Quiz(models.Model):
    # Example values
    # Purpose: prevent users from making quizes that will not be
    # playable (1 question with 1 option or inifnitely many options per question).
    # The purpose is to use these variables
    MIN_NUMBER_QUESTIONS = 3
    MAX_NUMBER_QUESTION = 20

    DIFFICULTY_LEVELS = (
        (0, 'easy'),
        (1, 'medium'),
        (2, 'hard'),
    )

    title = models.CharField(max_length=50, unique=True)
    description = models.CharField(
        max_length=200, blank=True, null=True,
        help_text="Few words about the quiz"
    )
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    categories = models.ManyToManyField(Category)
    difficulty = models.IntegerField(choices=DIFFICULTY_LEVELS)
    times_played = models.IntegerField(default=0)
    
    # Set to True after ViolationNtification was sent (see save method)
    # Usage: if true do not include this quiz in public listings (e.g. home page)
    under_moderation = models.BooleanField(default=False)
    # Set to True after mod decides that quiz is offending
    removed = models.BooleanField(default=False)

    def __str__(self):
        return f"Quiz {self.title}"

    def top_scores(self, limit=10):
        return self.scores.order_by('-score_value')[:limit]


class Question(models.Model):
    # Example values
    # Purpose: see Quiz class
    MIN_NUMBER_OPTIONS = 2
    MAX_NUMBER_OPTIONS = 4

    question_text = models.CharField(max_length=100)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name="questions")

    def __str__(self):
        return f"Question {self.question_text}"


class Option(models.Model):
    answer_text = models.CharField(max_length=100)
    is_correct = models.BooleanField(default=False)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name="options")

    def __str__(self):
        return f"Option {self.answer_text}"


class QuizScore(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name="scores")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    score_value = models.FloatField(default=0.0)
    date_finished = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Score {self.pk}"

    def save(self, *args, **kwargs):
        super(QuizScore, self).save(*args, **kwargs)
        # Update number of games for this quiz
        related_quiz = Quiz.objects.get(id=self.quiz.id)
        related_quiz.times_played = F('times_played') + 1
        related_quiz.save()
        # Update user's total score
        related_profile = User.objects.get(id=self.user.id).profile
        related_profile.total_score = F('total_score') + self.score_value
        related_profile.save()


class ViolationNotification(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    description = models.TextField(max_length=400)
    timestamp = models.DateTimeField(auto_now_add=True)
    is_closed = models.BooleanField(default=False)
    assigned_to = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def _choose_moderator(self):
        '''Assigns notification to moderator with the least active notifications.'''
        moderators = User.objects.filter(profile__is_moderator=True).all()
        number_assigned_per_mod = min([
            (len(mod.profile.assigned_notifications(active=True)), mod.pk)
            for mod in moderators
        ])
        return User.objects.get(id=number_assigned_per_mod[1])


    def save(self, *args, **kwargs):
        closing = False
        # Need to do this, because super().save() raises
        # an error with additional kwargs passed in.
        try:
            closing = kwargs.pop('closing')
        except KeyError:
            pass
        if not closing:
            # Assign to moderator, only when creating new notification
            mod = self._choose_moderator()
            self.assigned_to = mod
        super(ViolationNotification, self).save(*args, **kwargs)
        if not closing:
            # Set moderation flag, only when creating new notification
            related_quiz = Quiz.objects.get(id=self.quiz.id)
            related_quiz.under_moderation = True
            related_quiz.save()

    def dismiss(self):
        self.is_closed = True
        self.save(closing=True)
        related_quiz = Quiz.objects.get(id=self.quiz.id)
        related_quiz.under_moderation = False
        related_quiz.save()

    def accept(self):
        self.is_closed = True
        self.save(closing=True)
        related_quiz = Quiz.objects.get(id=self.quiz.id)
        related_quiz.under_moderation = False
        related_quiz.removed = True
        related_quiz.save()


    def __str__(self):
        return f"Violation notification {self.pk}"

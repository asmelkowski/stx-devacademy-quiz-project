from django.urls import path
from quiz.views import (
    FrontPageView, QuizDetailView, CategoryDetailView, QuizGameView,
    CalculateGameScore, ViolationsListView, ViolationsDetailView,
    ViolationReportCreateView
)

urlpatterns = [
    path("", FrontPageView.as_view(), name="index"),
    path("quiz/<int:pk>", QuizDetailView.as_view(), name="quiz_detail"),
    path("quiz/<int:pk>/play", QuizGameView.as_view(), name="quiz_game"),
    path("quiz/<int:pk>/calculate", CalculateGameScore.as_view(), name="calculate_score"),
    path("category/<int:pk>", CategoryDetailView.as_view(), name="category_detail"),
    path("violations", ViolationsListView.as_view(), name="violations_list"),
    path("violation/<int:pk>", ViolationsDetailView.as_view(), name="violation_detail"),
    path("report-abuse/<int:pk>", ViolationReportCreateView.as_view(), name="report_abuse"),
]
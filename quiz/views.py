import json
from django.http import request
from django.urls import reverse_lazy
from django.shortcuts import render, redirect, reverse
from django.views.generic import ListView, DetailView, View, CreateView
from quiz.models import Quiz, Category, Option, QuizScore, User, ViolationNotification
from django.contrib.auth.mixins import LoginRequiredMixin

from users.mixins import ModeratorOnlyMixin
# Create your views here.


class FrontPageView(ListView):
    model = Quiz
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['most_popular'] = Quiz.objects.filter(removed=False).filter(under_moderation=False).order_by('-times_played')[:5]
        return context


class QuizDetailView(DetailView):
    model = Quiz
    template_name = "quiz_detail.html"


class CategoryDetailView(DetailView):
    model = Category
    template_name = "category_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['other_categories'] = Category.objects.exclude(pk=self.kwargs['pk'])
        return context


class QuizGameView(LoginRequiredMixin, DetailView):
    login_url = '/users/login'
    model = Quiz
    template_name = "quiz_game.html"


class CalculateGameScore(View):
    def post(self, request, *args, **kwargs):
        points = 0
        game_data = json.loads(request.body)['formData']
        for answer in game_data:
            option = Option.objects.get(id=answer['option_id'])
            if option.is_correct:
                points += 100/answer['answer_time']
        new_quiz_score = QuizScore(
            quiz=Quiz.objects.get(pk=self.kwargs['pk']),
            user=User.objects.get(id=request.user.id),
            score_value=points,
        )
        new_quiz_score.save()
        return redirect('quiz:quiz_detail', pk=self.kwargs['pk'])


class ViolationsListView(ModeratorOnlyMixin, ListView):
    model = ViolationNotification
    template_name = 'violations_list.html'
    context_object_name = 'violations'

    def get_queryset(self):
        user = self.request.user
        if not user.profile.is_moderator:
            return None
        return user.profile.assigned_notifications(active=True)


class ViolationsDetailView(ModeratorOnlyMixin, DetailView):
    model = ViolationNotification
    template_name = 'violation_detail.html'
    context_object_name = 'violation'

    def post(self, request, *args, **kwargs):
        report_id = int(self.request.POST.get('id'))
        decision = self.request.POST.get('decision').lower()
        
        violation_report = ViolationNotification.objects.get(id=report_id)
        if violation_report is not None:
            if decision == 'accept':
                violation_report.accept()

            if decision == 'dismiss':
                violation_report.dismiss()

        return redirect('quiz:violations_list')

class ViolationReportCreateView(CreateView):
    model = ViolationNotification
    fields = ('description',)
    template_name = 'report_abuse.html'

    def get_success_url(self):
        return reverse('quiz:index')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['quiz'] = Quiz.objects.get(id=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        quiz = Quiz.objects.get(id=self.kwargs['pk'])
        self.object = form.save(commit=False)
        self.object.quiz = quiz
        return super(ViolationReportCreateView, self).form_valid(form)

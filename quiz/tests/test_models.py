from django.test import TestCase
from django.contrib.auth.models import User

from quiz.models import Category
from users.models import Profile


def create_category(category_name):
    return Category.objects.create(name=category_name)


class CategoryModelTests(TestCase):
    def test_category_creation(self):
        category = create_category('new test category')
        self.assertTrue(isinstance(category, Category), msg='Category should be create properly')
        self.assertEqual(category.__str__(), f'Category {category.name}', msg='Category string name is not equal to expected')

    def test_category_creation_with_too_long_name(self):
        catetory = create_category('my new name'*50)
        self.assertFalse(isinstance(catetory, Category), msg='Category shouldn\'t be created, too many characters')

    def test_category_creation_with_out_name(self):
        category = create_category('')
        self.assertFalse(isinstance(category, Category), msg='Category without name shouldn\'t be created')


class ProfileModelTests(TestCase):
    def test_automate_profile_creation(self):
        self.user = User.objects.create_user(username='user1', email='user1@stxnext.pl', password='user1')
        self.user2 = User.objects.create_user(username='user2', email='user2@stxnext.pl', password='user2')
        self.assertTrue(Profile.objects.filter(user=self.user), msg='User profile was not created')
